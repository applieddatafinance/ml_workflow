import pandas as pd
import numpy as np

def gains_table(
    y_true,
    y_pred,
    n_bins=10,
    ret_ks=False
): 
    """
    Build and return Gains Table
    
    Parameters
    ----------
    y_true : 1d array-like, or label indicator array / sparse matrix
        Ground truth (correct) target values.
    y_pred : 1d array-like, or label indicator array / sparse matrix
        Estimated targets as returned by a classifier.
    n_bins : int, default=10
        Number of bins for the gains_table
    ret_ks : bool, default=False
        If True, returns ks value along with gains_table.
    
    Returns
    -------
    gains_table : DataFrame
        
    ks : float
        Returned only if `ret_ks` is True.
    """
    
    df = pd.DataFrame()
    df['score'] = y_pred
    df['bad'] = y_true
    df['good'] = 1 - y_true

    df['bucket'] = pd.qcut(df.score.rank(method='first'), n_bins)
#     temp['bucket'] = pd.cut(temp.score, bins=[temp.score.min()-0.001,  temp.score.quantile(0.1),
#                                               temp.score.quantile(0.2),temp.score.quantile(0.3), 
#                                               temp.score.quantile(0.4), temp.score.quantile(0.5),
#                                               temp.score.quantile(0.6), temp.score.quantile(0.7), 
#                                               temp.score.quantile(0.8), temp.score.quantile(0.9), 
#                                               temp.score.quantile(1)], duplicates='drop')

    grouped = df.groupby('bucket', as_index=False)

    gains_table = pd.DataFrame()
    gains_table['min_score'] = grouped.min().score
    gains_table['max_score'] = grouped.max().score
    gains_table['n_bads'] = grouped.sum().bad
    gains_table['n_goods'] = grouped.sum().good
    gains_table['n_total'] = gains_table.n_bads + gains_table.n_goods

    gains_table['odds'] = (gains_table.n_goods / gains_table.n_bads).apply('{0:.2f}'.format)
    gains_table['bad_rate'] = (gains_table.n_bads / gains_table.n_total).apply('{0:.2%}'.format)
#     gains_table['good_rate'] = (gains_table.n_goods / gains_table.n_total).apply('{0:.2%}'.format)

    count_bads = df.bad.sum()
    count_goods = df.good.sum()
    gains_table['cs_bads'] = ((gains_table.n_bads / count_bads).cumsum()
                     * 100).apply('{0:.2f}'.format)
    gains_table['cs_goods'] = ((gains_table.n_goods / count_goods).cumsum()
                      * 100).apply('{0:.2f}'.format)
    gains_table['sep'] = np.abs(np.round(
        ((gains_table.n_bads / count_bads).cumsum() - (gains_table.n_goods / count_goods).cumsum()), 4) * 100)

    def flag(x): return '<----' if x == gains_table.sep.max() else ''
    gains_table['KS'] = gains_table.sep.apply(flag)

    ks = gains_table['sep'].max()
    
    if ret_ks :
        return gains_table, ks
    else:
        return gains_table
import setuptools

setuptools.setup(
    name='ml_workflow',
    url='',
    author='ADF',
    packages=['ml_workflow'],
    install_requires=['numpy','pandas'],
    version='0.1',
    description='',
    long_description=open('README.txt').read(),
)